﻿public interface IWeighted<T>
{
    float Weight { get; }
    T Item { get; }
}