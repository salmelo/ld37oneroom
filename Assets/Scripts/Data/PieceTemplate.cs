﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 86)]
public class PieceTemplate : ScriptableObject, IWeighted<PieceTemplate>
{
    public float frequency = 1;
    public List<SquareTemplate> squares;

    PieceTemplate IWeighted<PieceTemplate>.Item => this;
    float IWeighted<PieceTemplate>.Weight => frequency;

    public Piece GeneratePiece()
    {
        var p = new Piece() { Rotation = Random.Range(0, 4) } ;
        foreach (var s in squares)
        {
            p.Squares.Add(s.GenerateCoordSquare());
        }
        return p;
    }
}

[System.Serializable]
public struct SquareTemplate
{
    public Coord coord;
    public bool n, w, e, s;

    public Square GenerateSquare()
    {
        var sq = new Square();
        if (n)
        {
            sq.Colors[0] = (SquareColor)Random.Range(0, 6);
            sq.Shapes[0] = (SquareShape)Random.Range(0, 4);
        }
        if (e)
        {
            sq.Colors[1] = (SquareColor)Random.Range(0, 6);
            sq.Shapes[1] = (SquareShape)Random.Range(0, 4);
        }
        if (s)
        {
            sq.Colors[2] = (SquareColor)Random.Range(0, 6);
            sq.Shapes[2] = (SquareShape)Random.Range(0, 4);
        }
        if (w)
        {
            sq.Colors[3] = (SquareColor)Random.Range(0, 6);
            sq.Shapes[3] = (SquareShape)Random.Range(0, 4);
        }
        return sq;
    }

    public CoordSquare GenerateCoordSquare()
    {
        return new CoordSquare() { Coord = coord, Square = GenerateSquare() };
    }
}
