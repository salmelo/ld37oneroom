﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square
{
    public SquareColor?[] Colors { get; } = new SquareColor?[4];
    public SquareShape[] Shapes { get; } = new SquareShape[4];

    public Square(params SquareColor?[] colors)
    {
        for (int i = 0; i < colors.Length; i++)
        {
            if (i >= 4) break;
            Colors[i] = colors[i];
        }
    }

    public Square Clone()
    {
        var ns = new Square(Colors);
        for (int i = 0; i < Shapes.Length; i++)
        {
            ns.Shapes[i] = Shapes[i];
        }
        return ns;
    }

    public Square Rotated(int rotation)
    {
        var newS = new Square();
        for (int i = 0; i < Shapes.Length; i++)
        {
            var newI = (i + rotation) % 4;
            newS.Shapes[newI] = Shapes[i];
            newS.Colors[newI] = Colors[i];
        }
        return newS;
    }
}

public enum SquareColor
{
    Red, Yellow, Green, Cyan, Blue, Magenta
}

public enum SquareShape
{
    Circle, Diamond, Star, Triangle
}