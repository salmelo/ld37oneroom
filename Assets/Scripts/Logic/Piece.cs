﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Piece
{
    public List<CoordSquare> Squares { get; } = new List<CoordSquare>();

    private int rotation;
    public int Rotation
    {
        get { return rotation; }
        set
        {
            value = value % 4;
            if (rotation == value) return;

            rotation = value;
            RotationChanged?.Invoke(this);
        }
    }

    public IEnumerable<CoordSquare> RotatedSquares
    {
        get
        {
            return from s in Squares
                   select new CoordSquare() { Coord = s.Coord.Spin(rotation), Square = s.Square.Rotated(rotation) };
        }
    }

    public event System.Action<Piece> RotationChanged;

    public IEnumerable<Coord> Occupies(Coord @base)
    {
        foreach (var s in Squares)
        {
            yield return @base + s.Coord.Spin(rotation);
        }
    }

    public Piece Clone()
    {
        var p = new Piece() { Rotation = rotation };
        p.Squares.AddRange(Squares.Select(s => s.Clone()));
        return p;
    }
}

public struct CoordSquare
{
    public Coord Coord { get; set; }
    public Square Square { get; set; }

    internal CoordSquare Clone()
    {
        return new CoordSquare() { Coord = this.Coord, Square = this.Square.Clone() };
    }
}