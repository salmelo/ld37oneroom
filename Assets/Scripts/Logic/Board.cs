﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Board
{
    readonly Dictionary<Piece, Coord> pieces = new Dictionary<Piece, Coord>();

    public int Width { get; } = 7;
    public int Height { get; } = 7;

    public IEnumerable<Piece> AllPieces => pieces.Keys.ToList();

    public int PieceCount => pieces.Count;

    public event System.Action PieceMoved;

    public bool Contains(Coord coord)
    {
        return coord.X >= 0 && coord.X < Width
            && coord.Y >= 0 && coord.Y < Height;
    }

    public bool Contains(Piece piece)
    {
        return pieces.ContainsKey(piece);
    }

    public Piece GetPiece(Coord coord)
    {
        return pieces.Keys.FirstOrDefault(p => GetCoords(p).Contains(coord));
    }

    public IEnumerable<Coord> GetCoords(Piece piece)
    {
        Coord start;
        if (!pieces.TryGetValue(piece, out start))
        {
            return Enumerable.Empty<Coord>();
        }
        return piece.Occupies(start);
    }

    public bool CanPlace(Piece piece, Coord coord, Piece ignore = null)
    {
        var coords = new HashSet<Coord>(piece.Occupies(coord));

        if (coords.Any(c => !Contains(c))) return false;

        ignore = ignore ?? piece;

        foreach (var p in pieces.Keys)
        {
            if (p == ignore) continue;
            if (GetCoords(p).Any(c => coords.Contains(c))) return false;
        }
        return true;
    }

    public IDictionary<Coord, Square> GetSquareMap()
    {
        return (from p in pieces
                from s in p.Key.RotatedSquares
                select new { square = s.Square, coord = s.Coord + p.Value }
                ).ToDictionary(s => s.coord, s => s.square);
    }

    public void SetPiece(Piece piece, Coord coord)
    {
        if (!CanPlace(piece, coord)) throw new System.ArgumentException("Cannot place piece.");

        pieces[piece] = coord;
        PieceMoved?.Invoke();
    }

    public void RemovePiece(Piece piece)
    {
        pieces.Remove(piece);
        PieceMoved?.Invoke();
    }

}
