﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Math = System.Math;

[System.Serializable]
public struct Coord
{
    /// <summary>
    /// Coord with X and Y of <see cref="System.Int32.MinValue"/>, used to represent a non-value.
    /// </summary>
    public static Coord Null { get { return new Coord(int.MinValue, int.MinValue); } }
    public static Coord Zero { get { return new Coord(0, 0); } }

    [SerializeField]
    int x, y;

    public int X { get { return x; } }
    public int Y { get { return y; } }

    public Coord(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Coord operator +(Coord a, Coord b)
    {
        return new Coord(a.X + b.X, a.Y + b.Y);
    }

    public static Coord operator -(Coord a, Coord b)
    {
        return new Coord(a.X - b.X, a.Y - b.Y);
    }

    public static Coord operator -(Coord c)
    {
        return new Coord(-c.X, -c.Y);
    }

    public static bool operator ==(Coord a, Coord b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Coord a, Coord b)
    {
        return !(a == b);
    }

    public static Coord operator *(Coord c, int scalar)
    {
        return new Coord(c.x * scalar, c.y * scalar);
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return $"({x}, {y})";
    }

    /// <summary>
    /// The eight cardinal directions as unit-Coords. Listed in scatter-template order. (NW, N, NE, W, E, SW, S, SE)
    /// </summary>
    public static System.Collections.Generic.IList<Coord> Directions { get; } =
        new System.Collections.Generic.List<Coord>(
            new Coord[] { new Coord(-1, 1), new Coord(0, 1), new Coord(1, 1),
                              new Coord(-1, 0),                  new Coord(1, 0),
                              new Coord(-1,-1), new Coord(0,-1), new Coord(1,-1) }).AsReadOnly();

    public static Coord Direction(Direction dir)
    {
        return Directions[(int)dir];
    }

    /// <summary>
    /// Same coordinates as <see cref="Directions"/> but re-ordered so orthogonal neighbors are listed before diagonal ones.
    /// Coords within each group have the same relative ordering as in <see cref="Directions"/>.
    /// </summary>
    public static Coord[] OrderedDirections
    {
        get
        {
            var dirs = Directions;
            return new Coord[] { dirs[1], dirs[3], dirs[4], dirs[6], dirs[0], dirs[2], dirs[5], dirs[7] };
        }
    }

    /// <summary>
    /// All orthogonal directions. Same relative order as in <see cref="Directions"/>.
    /// </summary>
    public static Coord[] OrthogonalDirections
    {
        get
        {
            var dirs = Directions;
            return new Coord[] { dirs[1], dirs[3], dirs[4], dirs[6] };
        }
    }

    /// <summary>
    /// All orthogonal directions. Same relative order as in <see cref="ClockwiseDirections"/>.
    /// </summary>
    public static Coord[] ClockwiseOrthogonalDirections
    {
        get
        {
            var dirs = Directions;
            return new Coord[] { dirs[1], dirs[4], dirs[6], dirs[3] };
        }
    }

    /// <summary>
    /// All diagonal directions. Same relative order as in <see cref="Directions"/>.
    /// </summary>
    public static Coord[] DiagonalDirections
    {
        get
        {
            var dirs = Directions;
            return new Coord[] { dirs[0], dirs[2], dirs[5], dirs[7] };
        }
    }

    /// <summary>
    /// Same Coords as <see cref="Directions"/>, in clockwise order starting at NW. 
    /// </summary>
    public static Coord[] ClockwiseDirections
    {
        get
        {
            var dirs = Directions;
            return new Coord[] { dirs[0], dirs[1], dirs[2], dirs[4], dirs[7], dirs[6], dirs[5], dirs[3] };
        }
    }

    public static Direction GetDirection(Coord coord)
    {
        var i = Directions.IndexOf(coord);
        if (i < 0) throw new System.ArgumentException("Coord is not a direction coord.");
        return (Direction)i;
    }

    /// <summary>
    /// All eight neigbhoring coordinates, listed in same order as <see cref="Directions"/>.
    /// </summary>
    public Coord[] Neighbors
    {
        get
        {
            var c = this;
            return Directions.Select(n => n + c).ToArray();
        }
    }

    public Coord Neighbor(Direction dir)
    {
        return Direction(dir) + this;
    }

    /// <summary>
    /// Same coordinates as <see cref="Neighbors"/> but re-ordered so orthogonal neighbors are listed before diagonal ones.
    /// Coords within each group have the same relative ordering as in <see cref="Neighbors"/>.
    /// </summary>
    public Coord[] OrderedNeighbors
    {
        get
        {
            var ns = Neighbors;
            return new Coord[] { ns[1], ns[3], ns[4], ns[6], ns[0], ns[2], ns[5], ns[7] };
        }
    }

    /// <summary>
    /// All orthogonal neighbors. Same relative order as in <see cref="Neighbors"/>.
    /// </summary>
    public Coord[] OrthogonalNeighbors
    {
        get
        {
            var ns = Neighbors;
            return new Coord[] { ns[1], ns[3], ns[4], ns[6] };
        }
    }

    /// <summary>
    /// All orthogonal neighbors. Same relative order as in <see cref="ClockwiseNeighbors"/>.
    /// </summary>
    public Coord[] ClockwiseOrthogonalNeighbors
    {
        get
        {
            var ns = Neighbors;
            return new Coord[] { ns[1], ns[4], ns[6], ns[3] };
        }
    }

    /// <summary>
    /// All diagonal neighbors. Same relative order as in <see cref="Neighbors"/>.
    /// </summary>
    public Coord[] DiagonalNeighbors
    {
        get
        {
            var ns = Neighbors;
            return new Coord[] { ns[0], ns[2], ns[5], ns[7] };
        }
    }

    /// <summary>
    /// Same coordinates as <see cref="Neighbors"/>, in clockwise order starting at NW. 
    /// </summary>
    public Coord[] ClockwiseNeighbors
    {
        get
        {
            var t = this;
            return ClockwiseDirections.Select(c => c + t).ToArray();
        }
    }

    public static int Manhattan(Coord a, Coord b)
    {
        return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
    }

    public static float Euclidean(Coord a, Coord b)
    {
        var x = (a.x - b.x);
        var y = (a.y - b.y);
        return (float)Math.Sqrt(x * x + y * y);
    }

    public static float SqrEuclidean(Coord a, Coord b)
    {
        var x = (a.x - b.x);
        var y = (a.y - b.y);
        return x * x + y * y;
    }

    public static int Chessboard(Coord a, Coord b)
    {
        return Math.Max(Math.Abs(a.x - b.x), Math.Abs(a.y - b.y));
    }

    public bool IsNeighbor(Coord other)
    {
        return Chessboard(this, other) == 1;
    }

    public bool IsOrthogonalNeighbor(Coord other)
    {
        return Manhattan(this, other) == 1;
    }

    public bool IsDiagonalNeighbor(Coord other)
    {
        return IsNeighbor(other) && !IsOrthogonalNeighbor(other);
    }

    /// <summary>
    /// This Coord reduced to a Direction Coord.
    /// Resulting X and Y are each 0 if the original is 0, else 1 or -1, corresponding to the Sign of the original.
    /// </summary>
    public Coord AsDirection()
    {
        return new Coord(Math.Sign(X), Math.Sign(Y));
    }
}

public enum Direction
{
    NW, N, NE, W, E, SW, S, SE,
    NorthWest = NW,
    North = N,
    NorthEast = NE,
    West = W,
    East = E,
    SouthWest = SW,
    South = S,
    SouthEast = SE,
}
