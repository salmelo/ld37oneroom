﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Layout
{
    public static float SquareSize { get; set; } = 1;
    /// <summary>
    /// Whether to "Flatten" coordinates for 3D.
    /// If true, resulting Vectors will lie along a flat horizontal plane. (The X,Z plane.)
    /// If false, vectors will be on the X,Y plane.
    /// </summary>
    public static bool Flat { get; set; } = false;

    public static IList<Vector3> Corners(Coord c, bool? flat = null, float? size = null, float? centerSize = null)
    {
        Vector2 center = new Vector2(c.X, c.Y) * (centerSize ?? size ?? SquareSize);
        var halfSize = (size ?? SquareSize) / 2;

        var corners = new Vector3[] { new Vector3(center.x - halfSize, center.y + halfSize),
                                      new Vector3(center.x + halfSize, center.y + halfSize),
                                      new Vector3(center.x + halfSize, center.y - halfSize),
                                      new Vector3(center.x - halfSize, center.y - halfSize)
                                    };

        return flat ?? Flat ? corners.Select(v => new Vector3(v.x, 0, v.y)).ToArray() : corners;
    }

    public static Vector3 CoordToWorld(Coord c, bool? flat = null, float? size = null)
    {
        var center = new Vector3(c.X, c.Y) * (size ?? SquareSize);
        return flat ?? Flat ? new Vector3(center.x, 0, center.y) : center;
    }

    public static Coord WorldToCoord(Vector3 p, bool? flat = null, float? size = null)
    {
        var shrunk = p / (size ?? SquareSize);
        var x = Mathf.RoundToInt(shrunk.x);
        var y = Mathf.RoundToInt(flat ?? Flat ? shrunk.z : shrunk.y);
        return new Coord(x, y);
    }

    public static Rect CoordToRect(Coord c, float? size = null)
    {
        var squareSize = size ?? SquareSize;
        var halfSize = squareSize / 2;
        return new Rect(c.X - halfSize, c.Y - halfSize, squareSize, squareSize);
    }
}
