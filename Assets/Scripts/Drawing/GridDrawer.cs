﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GridDrawer : CoordMesh
{
    public int width = 10, height = 10;
    public int startX, startY;

    protected override bool CoordsToDraw(out IEnumerable<Coord> coords, out IEnumerable<ColoredCoord> colored)
    {
        coords = from y in Enumerable.Range(startY, height)
                 from x in Enumerable.Range(startX, width)
                 select new Coord(x, y);
        colored = null;
        return true;
    }
}
