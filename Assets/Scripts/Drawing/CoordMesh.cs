﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class CoordMesh : MonoBehaviour
{
    public int tileWidth = 1, tileHeight = 1;
    public int tileX = 0, tileY = 0;

    [Range(0, 1)]
    public float texelInset = 0;

    public float size = 0;

    /// <summary>
    /// Generate list of coords to draw.
    /// </summary>
    /// <param name="coords">Plain list of coords.</param>
    /// <param name="colored">List of coords with a color.</param>
    /// <returns>True to use plain coords, false to use colored ones.</returns>
    protected abstract bool CoordsToDraw(out IEnumerable<Coord> coords, out IEnumerable<ColoredCoord> colored);

    protected virtual void Init() { }

    void Start()
    {
        Init();
        GenerateMesh();
    }

#if UNITY_EDITOR
    [ContextMenu("Generate Mesh")]
#endif
    public void GenerateMesh()
    {
        IEnumerable<Coord> coords;
        IEnumerable<ColoredCoord> colored;
        if (CoordsToDraw(out coords, out colored))
        {
            GenerateMesh(coords);
        }
        else
        {
            GenerateMesh(colored);
        }
    }

    private void GenerateMesh(IEnumerable<Coord> coords)
    {
        if (!coords.Any())
        {
            GetComponent<MeshFilter>().mesh = null;
            var collider = GetComponent<MeshCollider>();
            if (collider != null) collider.sharedMesh = null;
            return;
        }

        var size = this.size > 0 ? this.size : Layout.SquareSize;

        var verts = new List<Vector3>();
        var tris = new List<int>();
        var uvs = new List<Vector2>();

        var uvWidth = (1f / tileWidth);
        var uvHeight = (1f / tileHeight);

        float xInset = 0, yInset = 0;
        if (texelInset > 0)
        {
            var tex = GetComponent<Renderer>().sharedMaterial.mainTexture;
            xInset = texelInset * (1f / tex.width);
            yInset = texelInset * (1f / tex.height);
        }

        var uvCorners = new Vector2[]
        {
            new Vector2(tileX * uvWidth + xInset, tileY * uvHeight + uvHeight - yInset),
            new Vector2(tileX * uvWidth + uvWidth - xInset, tileY * uvHeight + uvHeight - yInset),
            new Vector2(tileX * uvWidth + uvWidth - xInset, tileY * uvHeight + yInset),
            new Vector2(tileX * uvWidth + xInset, tileY * uvHeight + yInset)
        };

        var v = 0;

        foreach (var c in coords)
        {
            var corners = Layout.Corners(c, size: size, centerSize: Layout.SquareSize);
            verts.Add(corners[0]);
            verts.Add(corners[1]);
            verts.Add(corners[2]);
            verts.Add(corners[3]);

            uvs.Add(uvCorners[0]);
            uvs.Add(uvCorners[1]);
            uvs.Add(uvCorners[2]);
            uvs.Add(uvCorners[3]);

            tris.Add(v);
            tris.Add(v + 1);
            tris.Add(v + 3);
            tris.Add(v + 1);
            tris.Add(v + 2);
            tris.Add(v + 3);

            v += 4;
        }

        var m = new Mesh();
        m.vertices = verts.ToArray();
        m.triangles = tris.ToArray();
        m.uv = uvs.ToArray();

        GetComponent<MeshFilter>().mesh = m;
        var col = GetComponent<MeshCollider>();
        if (col != null) col.sharedMesh = m;
    }

    private void GenerateMesh(IEnumerable<ColoredCoord> squares)
    {
        if (!squares.Any())
        {
            GetComponent<MeshFilter>().mesh = null;
            var collider = GetComponent<MeshCollider>();
            if (collider != null) collider.sharedMesh = null;
            return;
        }

        var size = this.size > 0 ? this.size : Layout.SquareSize;

        var verts = new List<Vector3>();
        var tris = new List<int>();
        var uvs = new List<Vector2>();
        var colors = new List<Color>();

        var uvWidth = (1f / tileWidth);
        var uvHeight = (1f / tileHeight);

        float xInset = 0, yInset = 0;
        if (texelInset > 0)
        {
            var tex = GetComponent<Renderer>().sharedMaterial.mainTexture;
            xInset = texelInset * (1f / tex.width);
            yInset = texelInset * (1f / tex.height);
        }

        var uvCorners = new Vector2[]
        {
            new Vector2(tileX * uvWidth + xInset, tileY * uvHeight + uvHeight - yInset),
            new Vector2(tileX * uvWidth + uvWidth - xInset, tileY * uvHeight + uvHeight - yInset),
            new Vector2(tileX * uvWidth + uvWidth - xInset, tileY * uvHeight + yInset),
            new Vector2(tileX * uvWidth + xInset, tileY * uvHeight + yInset)
        };

        var v = 0;

        foreach (var square in squares)
        {
            var corners = Layout.Corners(square.coord, size: size, centerSize: Layout.SquareSize);
            verts.Add(corners[0]);
            verts.Add(corners[1]);
            verts.Add(corners[2]);
            verts.Add(corners[3]);

            uvs.Add(uvCorners[0]);
            uvs.Add(uvCorners[1]);
            uvs.Add(uvCorners[2]);
            uvs.Add(uvCorners[3]);

            tris.Add(v);
            tris.Add(v + 1);
            tris.Add(v + 3);
            tris.Add(v + 1);
            tris.Add(v + 2);
            tris.Add(v + 3);

            colors.Add(square.color);
            colors.Add(square.color);
            colors.Add(square.color);
            colors.Add(square.color);

            v += 4;
        }

        var m = new Mesh();
        m.vertices = verts.ToArray();
        m.triangles = tris.ToArray();
        m.uv = uvs.ToArray();
        m.colors = colors.ToArray();

        GetComponent<MeshFilter>().mesh = m;
        var col = GetComponent<MeshCollider>();
        if (col != null) col.sharedMesh = m;
    }

    public struct ColoredCoord
    {
        public Coord coord;
        public Color color;

        public ColoredCoord(Coord coord, Color color)
        {
            this.coord = coord;
            this.color = color;
        }
    }
}
