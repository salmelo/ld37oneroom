﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPiece : MonoBehaviour
{
    private void Start()
    {
        var pd = GetComponent<PieceDrawer>();
        var piece = new Piece();
        piece.Squares.Add(new CoordSquare()
        {
            Coord = new Coord(0, 0),
            Square = new Square(null, SquareColor.Blue, null, SquareColor.Magenta)
        });
        piece.Squares.Add(new CoordSquare()
        {
            Coord = new Coord(1, 0),
            Square = new Square(SquareColor.Red, null, SquareColor.Blue, SquareColor.Green)
        });
        piece.Squares.Add(new CoordSquare()
        {
            Coord = new Coord(0, 1),
            Square = new Square(null, SquareColor.Yellow, SquareColor.Blue)
        });
        piece.Squares.Add(new CoordSquare()
        {
            Coord = new Coord(0, 2),
            Square = new Square(SquareColor.Red, SquareColor.Cyan, SquareColor.Blue)
        });
        pd.Piece = piece;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<PieceDrawer>().Piece.Rotation += 1;
        }
    }
}
