﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

public class CreateXMFile
{
    [MenuItem("Assets/Create/XML File", priority = 85)]
    static void DoIt()
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeInstanceID);
        if (path == "") path = "Assets";
        if (!Directory.Exists(path))
        {
            path = Path.GetDirectoryName(path);
        }
        path = Path.Combine(path, "Xml File.xml");
        path = AssetDatabase.GenerateUniqueAssetPath(path);

        File.WriteAllText(path, "");
        AssetDatabase.Refresh();

        var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);

        //ProjectWindowUtil.StartNameEditingIfProjectWindowExists(asset.GetInstanceID(), null, path, AssetPreview.GetMiniThumbnail(asset), null);
    }
}