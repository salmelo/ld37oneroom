﻿using UnityEngine;
using UnityEditor;

public static class CreateManagerMenu
{
    [MenuItem("Assets/Create Manager")]
    static void CreateManager()
    {
        var script = (MonoScript)Selection.activeObject;
        var go = new GameObject("$" + script.name, script.GetClass());
        int managerCount = 0;
        var rx = new System.Text.RegularExpressions.Regex("^\\$.+Manager", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        foreach (var o in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (rx.IsMatch(o.name)) managerCount += 1;
        }
        go.transform.SetSiblingIndex(managerCount - 1);

        Undo.RegisterCreatedObjectUndo(go, "Create " + script.name);
    }

    [MenuItem("Assets/Create Manager", true)]
    static bool CreateManagerValidation()
    {
        var script = Selection.activeObject as MonoScript;
        if (script == null) return false;
        return script.name.EndsWith("Manager");
    }
}