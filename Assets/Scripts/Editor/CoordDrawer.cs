﻿using UnityEngine;
using UnityEditor;
using Rect = UnityEngine.Rect;

[CustomPropertyDrawer(typeof(Coord))]
public class CoordDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var lWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = EditorStyles.label.CalcSize(new GUIContent("X")).x;

        // Calculate rects

        Rect minRect = new Rect(position.x, position.y, position.width / 2 - 10, position.height);
        Rect maxRect = new Rect(position.x + minRect.width + 5, position.y, minRect.width, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(minRect, property.FindPropertyRelative("x"));
        EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("y"));

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;
        EditorGUIUtility.labelWidth = lWidth;

        EditorGUI.EndProperty();
    }
}
