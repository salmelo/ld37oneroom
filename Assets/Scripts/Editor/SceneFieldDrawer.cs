﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomPropertyDrawer(typeof(SceneField))]
public class SceneFieldDrawer : PropertyDrawer
{
    private const float heightMultiplier = 3.35f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //base.OnGUI(position, property, label);

        var pathProp = property.FindPropertyRelative("path");
        var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(pathProp.stringValue);

        var inBuild = IsInBuild(pathProp.stringValue);
        if (!inBuild)
        {
            position.height /= heightMultiplier;
        }

        EditorGUI.BeginChangeCheck();
        var newScene = EditorGUI.ObjectField(position, label, oldScene, typeof(SceneAsset), false);

        if (EditorGUI.EndChangeCheck())
        {
            var newPath = AssetDatabase.GetAssetPath(newScene);
            pathProp.stringValue = newPath;
        }

        if (!inBuild)
        {
            position.y += position.height;
            position.height *= heightMultiplier - 1;
            position.x += EditorGUIUtility.labelWidth;
            position.width -= EditorGUIUtility.labelWidth;

            EditorGUI.HelpBox(position, "Scene not in Build.", MessageType.Warning);
        }

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (IsInBuild(property.FindPropertyRelative("path").stringValue))
            return base.GetPropertyHeight(property, label);

        return base.GetPropertyHeight(property, label) * heightMultiplier;
    }

    bool IsInBuild(string path)
    {
        if (string.IsNullOrEmpty(path))
        {
            return true;
        }
        else
        {
            var inBuild = false;
            foreach (var s in EditorBuildSettings.scenes)
            {
                if (path == s.path)
                {
                    inBuild = true;
                    break;
                }
            }
            return inBuild;
        }
    }
}