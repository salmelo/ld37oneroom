﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

[CustomPreview(typeof(PieceTemplate))]
public class PieceTemplateEditor : ObjectPreview
{
    const int ButtonSize = 16;

    public override bool HasPreviewGUI()
    {
        return target;
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        var template = (PieceTemplate)target;
        int minX = 0, maxX = 0, minY = 0, maxY = 0;

        if (template.squares != null)
        {
            foreach (var s in template.squares)
            {
                if (s.coord.X < minX) minX = s.coord.X;
                else if (s.coord.X > maxX) maxX = s.coord.X;
                if (s.coord.Y < minY) minY = s.coord.Y;
                else if (s.coord.Y > maxY) maxY = s.coord.Y;
            } 
        }

        var xCount = (maxX - minX + 1);
        var yCount = (maxY - minY + 1);
        var squareSize = Mathf.Min((r.width - ButtonSize * 2) / xCount
                                , (r.height - ButtonSize * 2) / yCount);
        var baseRect = new Rect(r.position + Vector2.one * ButtonSize, Vector2.one * squareSize);
        Vector2 center = r.center;
        var centerCoord = new Coord(minX + xCount / 2, minY + yCount / 2);

        if ((xCount & 1) == 0)
        {
            center += Vector2.left * baseRect.height / 2;
            centerCoord = new Coord(centerCoord.X - 1, centerCoord.Y);
        }
        if ((yCount & 1) == 0)
        {
            center += Vector2.up * baseRect.height / 2;
            centerCoord = new Coord(centerCoord.X, centerCoord.Y - 1);
        }

        var occupied = new HashSet<Coord>();
        var neighbors = new HashSet<Coord>();

        if (template.squares != null)
        {
            for (int i = 0; i < template.squares.Count; i++)
            {
                var s = template.squares[i];
                var offsetCoord = s.coord - centerCoord;
                var boxRect = new Rect(baseRect);
                boxRect.center = center
                                + Vector2.down * offsetCoord.Y * squareSize
                                + Vector2.right * offsetCoord.X * squareSize;
                GUI.Box(boxRect, i.ToString());
                occupied.Add(s.coord);
                foreach (var n in s.coord.OrthogonalNeighbors)
                {
                    neighbors.Add(n);
                }
            }
        }

        var centerRect = new Rect(baseRect);
        centerRect.size = Vector2.one * squareSize / 2.5f;
        centerRect.center = center
                            + Vector2.down * -centerCoord.Y * squareSize
                            + Vector2.right * -centerCoord.X * squareSize;
        var bg = GUI.color;
        GUI.color = Color.green;
        GUI.Box(centerRect, GUIContent.none);
        GUI.color = bg;

        var so = new SerializedObject(target);
        var listProp = so.FindProperty("squares");

        for (int i = 0; i < listProp.arraySize; i++)
        {
            var nProp = listProp.GetArrayElementAtIndex(i).FindPropertyRelative("n");
            nProp.boolValue = !occupied.Contains(template.squares[i].coord.Neighbor(Direction.N));

            var sProp = listProp.GetArrayElementAtIndex(i).FindPropertyRelative("s");
            sProp.boolValue = !occupied.Contains(template.squares[i].coord.Neighbor(Direction.S));

            var eProp = listProp.GetArrayElementAtIndex(i).FindPropertyRelative("e");
            eProp.boolValue = !occupied.Contains(template.squares[i].coord.Neighbor(Direction.E));

            var wProp = listProp.GetArrayElementAtIndex(i).FindPropertyRelative("w");
            wProp.boolValue = !occupied.Contains(template.squares[i].coord.Neighbor(Direction.W));

            var buttonRect = new Rect(Vector2.zero, Vector2.one * ButtonSize);
            var offsetCoord = template.squares[i].coord - centerCoord;
            buttonRect.center = center
                            + Vector2.down * offsetCoord.Y * squareSize
                            + Vector2.right * offsetCoord.X * squareSize;
            if (GUI.Button(buttonRect, "-"))
            {
                listProp.DeleteArrayElementAtIndex(i);
            }
        }

        foreach (var n in neighbors.Where(c => !occupied.Contains(c)))
        {
            var buttonRect = new Rect(Vector2.zero, Vector2.one * ButtonSize);
            var offsetCoord = n - centerCoord;
            buttonRect.center = center
                            + Vector2.down * offsetCoord.Y * squareSize
                            + Vector2.right * offsetCoord.X * squareSize;

            if (n.X < minX) buttonRect.center += Vector2.right * (squareSize / 2 - ButtonSize / 2);
            else if (n.X > maxX) buttonRect.center += Vector2.left * (squareSize / 2 - ButtonSize / 2);

            if (n.Y < minY) buttonRect.center += Vector2.down * (squareSize / 2 - ButtonSize / 2);
            else if (n.Y > maxY) buttonRect.center += Vector2.up * (squareSize / 2 - ButtonSize / 2);

            if (GUI.Button(buttonRect, "+"))
            {
                listProp.InsertArrayElementAtIndex(listProp.arraySize);
                var newProp = listProp.GetArrayElementAtIndex(listProp.arraySize - 1);
                newProp.FindPropertyRelative("coord.x").intValue = n.X;
                newProp.FindPropertyRelative("coord.y").intValue = n.Y;
            }
        }

        if (listProp.arraySize == 0) listProp.InsertArrayElementAtIndex(0);

        so.ApplyModifiedProperties();
    }
}