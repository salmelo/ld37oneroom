﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PieceDrawer : MonoBehaviour, IPointerClickHandler
{
    public SquareDrawer squarePrefab;

    private Piece piece;
    public Piece Piece
    {
        get { return piece; }
        set
        {
            if (piece != null) UnsetEvents();
            piece = value;
            if (started)
            {
                DrawPiece();
                SetEvents();
            }
        }
    }

    public PieceEvent pieceClicked;
    public PieceEvent pieceRightClicked;

    private bool started;

    // Use this for initialization
    void Start()
    {
        if (Piece != null)
        {
            DrawPiece();
            SetEvents();
        }
        started = true;
    }

    private void DrawPiece()
    {
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }
        foreach (var s in Piece.Squares)
        {
            var square = Instantiate(squarePrefab, transform);
            square.transform.localPosition = Layout.CoordToWorld(s.Coord);
            square.Square = s.Square;
        }
        SetRotation();
    }

    private void SetEvents()
    {
        Piece.RotationChanged += Piece_RotationChanged;
    }

    private void Piece_RotationChanged(Piece obj)
    {
        if (obj != Piece) return;
        SetRotation();
    }

    private void SetRotation()
    {
        transform.rotation = Quaternion.AngleAxis(-90 * Piece.Rotation, Vector3.forward);
    }

    private void OnDestroy()
    {
        UnsetEvents();
    }

    private void UnsetEvents()
    {
        piece.RotationChanged -= Piece_RotationChanged;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left) pieceClicked.Invoke(Piece);
        else if (eventData.button == PointerEventData.InputButton.Right) pieceRightClicked.Invoke(Piece);
    }
}
