﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[DisallowMultipleComponent]
public class CoordMover : MonoBehaviour
{
    public float speed = 5;

    public string moveBool = "Moving";

    private Coroutine pathCoroutine;

    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        if (!anim) anim = null;
    }

    public void TeleportTo(Coord coord)
    {
        TeleportTo(Layout.CoordToWorld(coord).WithZ(transform.position.z));
    }

    public void TeleportTo(Vector3 pos)
    {
        StopPath();
        transform.position = pos;
    }

    public void MoveTo(Coord coord, System.Action doneCallback = null)
    {
        PathAlong(new Coord[] { coord }, doneCallback);
    }

    public void MoveTo(Vector3 pos, System.Action doneCallback = null)
    {
        PathAlong(new Vector3[] { pos }, doneCallback);
    }

    public void PathAlong(IEnumerable<Coord> path, System.Action doneCallback = null)
    {
        StopPath();
        pathCoroutine = StartCoroutine(PathRoutine(path, doneCallback));
    }

    public void PathAlong(IEnumerable<Vector3> path, System.Action doneCallback = null) 
    {
        StopPath();
        pathCoroutine = StartCoroutine(PathRoutine(path, doneCallback));
    }

    public void StopPath()
    {
        if (pathCoroutine != null)
        {
            StopCoroutine(pathCoroutine);
            pathCoroutine = null;

            anim?.SetBool(moveBool, false);
        }
    }

    private IEnumerator PathRoutine(IEnumerable<Coord> path, System.Action doneCallback)
    {
        return PathRoutine(path.Select(c => Layout.CoordToWorld(c).WithZ(transform.position.z)), doneCallback);
    }

    private IEnumerator PathRoutine(IEnumerable<Vector3> path, System.Action doneCallback)
    {
        anim?.SetBool(moveBool, true);

        foreach (var v in path)
        {
            var pos = v;
            while ((transform.position - pos).sqrMagnitude > 0.001f)
            {
                transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
                yield return null;
            }
        }

        anim?.SetBool(moveBool, false);
        doneCallback?.Invoke();
    }
}
