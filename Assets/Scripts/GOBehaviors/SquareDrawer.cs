﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareDrawer : MonoBehaviour
{
    public Square Square { get; set; }

    public SpriteRenderer[] triangles;
    public SpriteRenderer[] shapes;

    private void Awake()
    {
        if (triangles.Length != 4) Debug.LogError("SquareDrawer should have 4 triangles.");
        if (shapes.Length != 4) Debug.LogError("SquareDrawer should have 4 shapes.");
    }

    // Use this for initialization
    void Start()
    {
        UpdateTriangles();
    }

    public void UpdateTriangles()
    {
        for (int i = 0; i < triangles.Length; i++)
        {
            triangles[i].color = Square.Colors[i]?.ToColor() ?? Color.gray;
        }

        for (int i = 0; i < shapes.Length; i++)
        {
            shapes[i].sprite = GraphicsManager.Current.ShapeToSprite(Square.Shapes[i]);
            //shapes[i].color = Square.Colors[i]?.Complementary().ToColor() ?? Color.gray;
            if (!Square.Colors[i].HasValue) shapes[i].enabled = false;
        }
    }
}
