﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PieceGrabber : MonoBehaviour
{
    public Transform target;
    public float speed = 7;

    public event System.Action<PieceGrabber> OnTargetHit;

    private Rigidbody2D targetRB;
    private Rigidbody2D rb;
    private Animator anim;

    private Vector3 home;
    private bool hitTarget;
    private Vector3 targetOffset;
    private bool goHome;

    private Vector3 lastPosition;

    // Use this for initialization
    void Start()
    {
        targetRB = target.GetComponent<Rigidbody2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        transform.rotation = Utils2D.LookRotation2D(target.position - transform.position);
        home = transform.position;
    }

    private void Update()
    {
        lastPosition = transform.position;

        if (goHome)
        {
            transform.position = Vector3.MoveTowards(transform.position, home, speed * Time.deltaTime);
            if ((transform.position - home).sqrMagnitude < 0.001f)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            if (hitTarget)
            {
                transform.position = target.position + targetOffset;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position
                                                        , target.position.WithZ(transform.position.z)
                                                        , speed * Time.deltaTime);
            }
        }

        anim.SetBool("Walking", (lastPosition - transform.position).sqrMagnitude > 0.0001f);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (hitTarget) return;
        if (collision.attachedRigidbody == targetRB)
        {
            hitTarget = true;
            targetOffset = transform.position - target.position;
            OnTargetHit?.Invoke(this);
            anim.SetBool("Grabbing", true);
        }
    }

    public void GoHome()
    {
        goHome = true;
        anim.SetBool("Grabbing", false);
        transform.rotation = Utils2D.LookRotation2D(home - transform.position);
    }
}
