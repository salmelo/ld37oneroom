﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverScore : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        var t = GetComponent<Text>();
        var afterLevelData = FindObjectOfType<AfterLevelData>();
        t.text = t.text.Replace("$$$", afterLevelData.score.ToString()).Replace("XX", afterLevelData.made.ToString());
        Destroy(afterLevelData.gameObject);
    }
}
