﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpriteChildTinter : MonoBehaviour
{
    [SerializeField]
    private Color color = Color.white;
    public Color Color
    {
        get { return color; }
        set
        {
            if (color == value) return;
            color = value;
            if (started) TintSprites();
        }
    }

    private Dictionary<SpriteRenderer, Color> originalColors = new Dictionary<SpriteRenderer, Color>();
    private bool started;

    private void Start()
    {
        if (color != Color.white)
        {
            //tint sprites next frame, after the squares have done their coloring.
            var t = new System.Threading.Tasks.Task(
                () => (new System.Threading.Tasks.Task(TintSprites)).Start(UnityScheduler.UpdateScheduler)
                );
            t.Start(UnityScheduler.LateUpdateScheduler);
        }
        started = true;
    }

    private void TintSprites()
    {
        var sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var s in sprites)
        {
            var col = originalColors.GetOrAdd(s, s.color);
            s.color = col * color;
        }
    }
}
