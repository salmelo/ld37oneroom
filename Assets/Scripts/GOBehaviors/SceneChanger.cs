﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public SceneField scene;

    public void ChangeScene()
    {
        SceneManager.LoadScene(scene.Path);
    }
}
