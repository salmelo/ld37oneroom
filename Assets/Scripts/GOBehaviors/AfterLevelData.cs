﻿using UnityEngine;
using System.Collections;

public class AfterLevelData : MonoBehaviour
{
    public int score;
    public int made;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
