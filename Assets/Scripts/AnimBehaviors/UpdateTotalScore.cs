﻿using UnityEngine;
using System.Collections;

public class UpdateTotalScore : StateMachineBehaviour
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        UIManager.Current.UpdateTotalScore();
    }
}
