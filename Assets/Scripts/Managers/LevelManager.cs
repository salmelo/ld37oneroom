﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Current { get; private set; }

    public PieceDrawer piecePrefab;
    public PieceTemplate[] pieceTemplates;
    public RectTransform inputZone;

    public float pieceGenOffset = 8;
    public float pieceExitX = 12;
    public float startLevelTime = 60;
    public float endLevelTime = 15;
    public int endLevelCount = 12;

    public SceneField gameOverScene;

    public Dictionary<Piece, PieceDrawer> PieceDrawers { get; } = new Dictionary<Piece, PieceDrawer>();
    public Board Board { get; } = new Board();
    public int Score { get; private set; }
    public float TimeLeft { get; private set; }
    public int Level { get; private set; }

    public bool CanFinish => Board.PieceCount >= PieceDrawers.Count;

    private Transform pieceContainer;

    private int piecesEntering;
    private int piecesLeaving;

    private bool timerRunning;

    private void Awake()
    {
        Current = this;
    }

    // Use this for initialization
    void Start()
    {
        pieceContainer = (new GameObject("Pieces")).transform;
        pieceContainer.SetParent(transform, true);

        var t = new System.Threading.Tasks.Task(StartLevel);
        t.Start(UnityScheduler.LateUpdateScheduler);

        Board.PieceMoved += UIManager.Current.UpdateCanFinish;
    }

    private void Update()
    {
        if (timerRunning)
        {
            TimeLeft -= Time.deltaTime;

            if (TimeLeft <= 0)
            {
                TimeLeft = 0;

                GameOver();
            }
        }
    }

    private void GameOver()
    {
        var afterData = new GameObject().AddComponent<AfterLevelData>();
        afterData.score = Score;
        afterData.made = Level - 1;

        SceneManager.LoadScene(gameOverScene.Path);
    }

    void StartLevel()
    {
        GeneratePieces(5);

        TimeLeft = Mathf.Lerp(startLevelTime, endLevelTime, Level / (float)endLevelCount);
        Level++;

        UIManager.Current.ToggleLeftConveyorBelt(true);
        UIManager.Current.GrowLeftConveyor();
        UIManager.Current.UpdateCanFinish();
    }

    void GeneratePieces(int count)
    {
        Vector3[] inputCorners = new Vector3[4];
        inputZone.GetWorldCorners(inputCorners);
        var minX = inputCorners.Min(v => v.x);
        var maxX = inputCorners.Max(v => v.x);
        var minY = inputCorners.Min(v => v.y);
        var maxY = inputCorners.Max(v => v.y);

        for (int i = 0; i < count; i++)
        {
            var template = pieceTemplates.SelectRandom();
            var piece = Instantiate(piecePrefab, pieceContainer);
            piece.Piece = template.GeneratePiece();
            var targetPos = (new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY))).WithZ(-i);
            piece.transform.position = targetPos.WithX(targetPos.x - pieceGenOffset);
            piece.pieceClicked.AddListener(UIManager.Current.SetActivePiece);

            piecesEntering += 1;
            piece.GetComponent<CoordMover>().MoveTo(targetPos, PieceDoneEntering);

            PieceDrawers.Add(piece.Piece, piece);
        }
    }

    public void StartEndLevel()
    {
        if (!CanFinish) return;

        timerRunning = false;

        foreach (var piece in PieceDrawers.Values)
        {
            piece.pieceClicked.RemoveAllListeners();
            piece.pieceRightClicked.RemoveAllListeners();
        }

        if (UIManager.Current.PiecesInTransit)
        {
            UIManager.Current.AllPiecesMoved += EndLevel;
        }
        else
        {
            EndLevel();
        }
    }

    private void EndLevel()
    {
        UIManager.Current.AllPiecesMoved -= EndLevel;

        UIManager.Current.GrowRightConveyor();
    }

    int CalculateScore()
    {
        var squares = Board.GetSquareMap();

        var score = 0;
        foreach (var pair in squares)
        {
            var s = pair.Value;
            var c = pair.Key;
            var neighbors = c.ClockwiseOrthogonalNeighbors;
            for (int i = 0; i < neighbors.Length; i++)
            {
                var shape = s.Shapes[i];
                var color = s.Colors[i];
                var n = squares.GetOrNull(neighbors[i]);

                if (n == null) continue;
                var nShape = n.Shapes[(i + 2) % 4];
                var nColor = n.Colors[(i + 2) % 4];

                if (color != null)
                {
                    if (shape == nShape)
                    {
                        score += 5;
                    }
                    if (color == nColor)
                    {
                        score += 5;
                    }
                    else if (color == nColor.Value.Complementary())
                    {
                        score += 5;
                    }
                }
            }
        }

        return score;
    }

    void PieceDoneEntering()
    {
        piecesEntering--;

        if (piecesEntering <= 0)
        {
            UIManager.Current.ToggleLeftConveyorBelt(false);
            UIManager.Current.ShrinkLeftConveyor();

            timerRunning = true;
        }
    }

    void PieceDoneLeaving(GameObject piece)
    {
        Destroy(piece);
        piecesLeaving--;

        if (piecesLeaving <= 0)
        {
            UIManager.Current.ShrinkRightConveyor();
            UIManager.Current.ToggleRightConveyorBelt(false);

            StartLevel();
        }
    }

    public void MoveFinishedPieces()
    {
        UIManager.Current.ToggleRightConveyorBelt(true);

        var points = CalculateScore();
        UIManager.Current.DisplayScore(points);
        Score += points;

        foreach (var piece in Board.AllPieces)
        {
            Board.RemovePiece(piece);
            var mover = PieceDrawers[piece].GetComponent<CoordMover>();
            mover.MoveTo(mover.transform.position.WithX(pieceExitX), () => PieceDoneLeaving(mover.gameObject));
            piecesLeaving++;

            PieceDrawers[piece].pieceClicked.RemoveAllListeners();
            PieceDrawers[piece].pieceRightClicked.RemoveAllListeners();
            PieceDrawers.Remove(piece);
        }
    }
}
