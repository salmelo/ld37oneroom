﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphicsManager : MonoBehaviour
{
    public static GraphicsManager Current { get; private set; }

    public Sprite triangleSprite;
    public Sprite circleSprite;
    public Sprite diamondSprite;
    public Sprite starSprite;

    private void Awake()
    {
        Current = this;
    }

    public Sprite ShapeToSprite(SquareShape shape)
    {
        switch (shape)
        {
            case SquareShape.Circle:
                return circleSprite;
            case SquareShape.Diamond:
                return diamondSprite;
            case SquareShape.Star:
                return starSprite;
            case SquareShape.Triangle:
                return triangleSprite;
            default:
                throw new System.ArgumentException("Invalid shape.");
        }
    }
}
