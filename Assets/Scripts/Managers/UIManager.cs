﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{

    public static UIManager Current { get; private set; }

    public RectTransform inputBox;

    public Animator leftConveyor;
    public Animator leftConveyorBelt;
    public Animator rightConveyor;
    public Animator rightConveyorBelt;

    public Text scoreText;
    public Text totalScoreText;
    public Text timerText;

    public Button finishButton;

    public PieceGrabber grabberPrefab;
    public Transform[] grabberSpawns;

    public Piece ActivePiece { get; private set; }

    private CoordMover activePieceDrawer;

    private Dictionary<Piece, int> piecesToGrab = new Dictionary<Piece, int>();
    private Dictionary<Piece, List<PieceGrabber>> piecesToGrabbers = new Dictionary<Piece, List<PieceGrabber>>();
    private Dictionary<Piece, Coord> piecesToMove = new Dictionary<Piece, Coord>();
    private Dictionary<PieceGrabber, Piece> grabbersToPieces = new Dictionary<PieceGrabber, Piece>();
    private Dictionary<Piece, PieceDrawer> piecesToPreviews = new Dictionary<Piece, PieceDrawer>();

    public bool PiecesInTransit => piecesToPreviews.Count > 0;
    public event System.Action AllPiecesMoved;

    private void Awake()
    {
        Current = this;
    }

    // Use this for initialization
    void Start()
    {
        rightConveyorBelt.speed = leftConveyorBelt.speed = 0;
    }

    public void SetActivePiece(Piece piece)
    {
        if (piecesToPreviews.ContainsKey(piece)) return;

        ActivePiece = piece;
        if (activePieceDrawer) Destroy(activePieceDrawer.gameObject);

        var pd = Instantiate(LevelManager.Current.piecePrefab, transform);
        activePieceDrawer = pd.GetComponent<CoordMover>();

        pd.Piece = piece.Clone();

        pd.GetComponent<SpriteChildTinter>().Color = new Color(1, 1, 1, .5f);

        pd.pieceClicked.AddListener(ActivePieceClicked);
        pd.pieceRightClicked.AddListener(ActivePieceRightClicked);

        pd.transform.position = pd.transform.position.WithZ(-9);
    }

    private void ActivePieceRightClicked(Piece piece)
    {
        piece.Rotation += 1;
    }

    private void ActivePieceClicked(Piece piece)
    {
        var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition).WithZ(0);
        var coord = Layout.WorldToCoord(mPos);
        if (LevelManager.Current.Board.Contains(coord))
        {
            if (LevelManager.Current.Board.CanPlace(piece, coord, ActivePiece))
            {
                ActivePiece.Rotation = piece.Rotation;
                LevelManager.Current.Board.SetPiece(ActivePiece, coord);
                StartPiecePlacement(piece, coord);
            }
        }
        else
        {
            var inBounds = inputBox.RectBounds();
            if (inBounds.Contains(mPos))
            {
                Destroy(activePieceDrawer.gameObject);
                var pd = LevelManager.Current.PieceDrawers[ActivePiece];
                pd.transform.position = mPos.WithZ(pd.transform.position.z);
                ActivePiece.Rotation = piece.Rotation;
            }

            if (LevelManager.Current.Board.Contains(ActivePiece))
            {
                LevelManager.Current.Board.RemovePiece(ActivePiece);
            }
        }
    }

    private void StartPiecePlacement(Piece piece, Coord coord)
    {
        piecesToMove[ActivePiece] = coord;
        piecesToGrab[ActivePiece] = grabberSpawns.Length;

        var pd = activePieceDrawer.GetComponent<PieceDrawer>();
        piecesToPreviews[ActivePiece] = pd;
        pd.pieceRightClicked.RemoveAllListeners();
        pd.pieceClicked.RemoveAllListeners();
        activePieceDrawer = null;

        var grabberList = new List<PieceGrabber>(grabberSpawns.Length);
        foreach (var spawn in grabberSpawns)
        {
            var grabber = Instantiate(grabberPrefab, transform);
            grabbersToPieces[grabber] = ActivePiece;

            grabber.transform.position = spawn.position;
            grabber.target = LevelManager.Current.PieceDrawers[ActivePiece].transform;
            grabber.OnTargetHit += Grabber_OnTargetHit;

            grabberList.Add(grabber);
        }
        piecesToGrabbers.Add(ActivePiece, grabberList);

        ActivePiece = null;
    }

    private void Grabber_OnTargetHit(PieceGrabber grabber)
    {
        var piece = grabbersToPieces[grabber];
        grabbersToPieces.Remove(grabber);

        piecesToGrab[piece]--;
        if (piecesToGrab[piece] <= 0)
        {
            LevelManager.Current.PieceDrawers[piece].GetComponent<CoordMover>()
                .MoveTo(piecesToMove[piece], () => PieceMoved(piece));
            piecesToMove.Remove(piece);
            piecesToGrab.Remove(piece);
        }
    }

    private void PieceMoved(Piece piece)
    {
        foreach(var g in piecesToGrabbers[piece])
        {
            g.GoHome();
        }
        piecesToGrabbers.Remove(piece);

        Destroy(piecesToPreviews[piece].gameObject);
        piecesToPreviews.Remove(piece);

        if (!PiecesInTransit)
        {
            AllPiecesMoved?.Invoke();
        }
    }

    private void Update()
    {
        if (activePieceDrawer != null)
        {
            var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition).WithZ(-9);
            var coord = Layout.WorldToCoord(mPos);
            if (LevelManager.Current.Board.Contains(coord))
            {
                activePieceDrawer.TeleportTo(coord);
                activePieceDrawer.GetComponent<SpriteChildTinter>().Color
                    = LevelManager.Current.Board
                        .CanPlace(activePieceDrawer.GetComponent<PieceDrawer>().Piece, coord, ActivePiece)
                    ? new Color(1, 1, 1, .5f)
                    : new Color(.25f, .25f, .25f, .5f);
            }
            else
            {
                activePieceDrawer.transform.position = mPos;
                activePieceDrawer.GetComponent<SpriteChildTinter>().Color = new Color(1, 1, 1, .5f);
            }
        }

        var t = System.TimeSpan.FromSeconds(LevelManager.Current.TimeLeft);
        timerText.text = $"Time: {t.Minutes}:{t.Seconds:00}:{Mathf.RoundToInt(t.Milliseconds / 10):00}";
    }

    public void GrowRightConveyor()
    {
        rightConveyor.SetBool("Grow", true);
    }

    public void ShrinkRightConveyor()
    {
        rightConveyor.SetBool("Grow", false);
    }

    public void GrowLeftConveyor()
    {
        leftConveyor.SetBool("Grow", true);
    }

    public void ShrinkLeftConveyor()
    {
        leftConveyor.SetBool("Grow", false);
    }

    public void ToggleRightConveyorBelt(bool on)
    {
        rightConveyorBelt.speed = on ? 1 : 0;
    }

    public void ToggleLeftConveyorBelt(bool on)
    {
        leftConveyorBelt.speed = on ? 1 : 0;
    }

    public void UpdateTotalScore()
    {
        totalScoreText.text = $"${LevelManager.Current.Score}";
    }

    public void DisplayScore(int amount)
    {
        scoreText.text = $"${amount}";
        scoreText.GetComponent<Animator>().SetTrigger("Grow");
    }

    public void UpdateCanFinish()
    {
        finishButton.interactable = LevelManager.Current.CanFinish;
    }
}
