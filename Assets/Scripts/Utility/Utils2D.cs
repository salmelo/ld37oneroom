﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utils2D
{

    public static Quaternion LookRotation2D(Vector2 forward, bool rightIsForward = false)
    {
        float angle = Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg;

        if (!rightIsForward) angle -= 90;

        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
    
}
