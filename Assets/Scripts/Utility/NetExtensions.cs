﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public static class NetExtensions
{
    public static T GetOrNull<TKey, T>(this IDictionary<TKey, T> dict, TKey key) where T : class
    {
        return GetOrDefault(dict, key, null);
    }

    public static T? GetOrNullStruct<TKey, T>(this IDictionary<TKey, T> dict, TKey key) where T : struct
    {
        T value;
        if (!dict.TryGetValue(key, out value))
        {
            return null;
        }

        return value;
    }

    public static T GetOrDefault<TKey, T>(this IDictionary<TKey, T> dict, TKey key, T def)
    {
        T value;
        if (!dict.TryGetValue(key, out value))
        {
            return def;
        }
        return value;
    }

    public static T GetOrDefault<TKey, T>(this IDictionary<TKey, T> dict, TKey key)
    {
        return GetOrDefault(dict, key, default(T));
    }

    public static T GetOrAdd<TKey, T>(this IDictionary<TKey, T> dict, TKey key, T def)
    {
        return dict[key] = GetOrDefault(dict, key, def);
    }

    //public static T GetOrAdd<TKey, T>(this IDictionary<TKey, T> dict, TKey key) where T : class
    //{
    //    return dict[key] = GetOrDefault(dict, key);
    //}

    public static T GetOrAdd<TKey, T>(this IDictionary<TKey, T> dict, TKey key, Func<T> generator) 
    {
        T value;
        if (dict.TryGetValue(key, out value))
        {
            return value;
        }
        return dict[key] = generator();
    }

    public static T GetOrAddNew<TKey, T>(this IDictionary<TKey, T> dict, TKey key) where T : new()
    {
        return GetOrAdd(dict, key, () => new T());
    }

    public static T AddOrChange<TKey, T>(this Dictionary<TKey, T> dict, TKey key, T add, Func<T, T> changer)
    {
        T value;
        if (dict.TryGetValue(key, out value))
        {
            return dict[key] = changer(value);
        }
        return dict[key] = add;
    }

    public static T AddOrChange<TKey, T>(this Dictionary<TKey, T> dict, TKey key, Func<T> adder, Func<T, T> changer)
    {
        T value;
        if (dict.TryGetValue(key, out value))
        {
            return dict[key] = changer(value);
        }
        return dict[key] = adder();
    }

    public static T GetOrThrow<TKey, T>(this IDictionary<TKey, T> dict, TKey key, Exception e)
    {
        T value;
        if (dict.TryGetValue(key, out value))
        {
            return value;
        }
        throw e;
    }

    public static T GetOrThrow<TKey, T>(this IDictionary<TKey, T> dict, TKey key, Func<Exception, Exception> exFunc)
    {
        try { return dict[key]; }
        catch (KeyNotFoundException e)
        {
            throw exFunc(e);
        }
    }

    //public static T LastOrDefault<T>(this IEnumerable<T> list, T def, System.Func<T, bool> predicate = null)
    //{
    //    try
    //    {
    //        if (predicate == null) return list.Last();
    //        else return list.Last(predicate);
    //    }
    //    catch (System.InvalidOperationException)
    //    {
    //        return def;
    //    }
    //}

    public static IEnumerable<TResult> NonNulls<TResult, TList>(this IEnumerable<TList> list, Func<TList, TResult> func) where TResult : class
    {
        return from t in list
               let r = func(t)
               where r != null
               select r;
    }

    public static IEnumerable<T> NonNulls<T>(this IEnumerable<T> list) where T : class
    {
        return list.NonNulls(t => t);
    }

    public static bool None<TSource>(this IEnumerable<TSource> source)
    {
        return !source.Any();
    }

    public static bool None<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
    {
        return !source.Any(predicate);
    }

    public static bool MoreThanOne<T>(this IEnumerable<T> source)
    {
        var enumerator = source.GetEnumerator();
        if (enumerator.MoveNext() == false) return false;
        return enumerator.MoveNext();
    }

    public static T Last<T>(this IList<T> list)
    {
        return list[list.Count - 1];
    }

    public static int LastIndex<T>(this IList<T> list)
    {
        return list.Count - 1;
    }

    public static bool IsNullOr<T>(this T obj, T other) where T : class
    {
        return obj == null || obj.Equals(other);
    }

    public static bool ContainsAny<T>(this IEnumerable<T> list, IEnumerable<T> lookFor, IEqualityComparer<T> comparer = null)
    {
        comparer = comparer ?? EqualityComparer<T>.Default;
        var set = new HashSet<T>(lookFor, comparer);

        var enumerator = list.GetEnumerator();

        while (enumerator.MoveNext())
        {
            if (set.Contains(enumerator.Current)) return true;
        }

        return false;
    }

    public static IEnumerable<U> Scan<T, U>(this IEnumerable<T> input, U seed, Func<U, T, U> next)
    {
        foreach (var item in input)
        {
            seed = next(seed, item);
            yield return seed;
        }
    }

    public static IEnumerable<U> Scan<T, U>(this IEnumerable<T> input, U seed, Func<U, T, U> next, bool includeSeed = false)
    {
        if (includeSeed) yield return seed;
        foreach (var item in input)
        {
            seed = next(seed, item);
            yield return seed;
        }
    }

    public static IEnumerable<T> Scan<T>(this IEnumerable<T> input, Func<T, T, T> next)
    {
        var enumerator = input.GetEnumerator();
        if (!enumerator.MoveNext()) yield break;

        var acc = enumerator.Current;
        yield return acc;

        while (enumerator.MoveNext())
        {
            acc = next(acc, enumerator.Current);
            yield return acc;
        }
    }

    public static T? LastOrNull<T>(this IEnumerable<T> list) where T : struct
    {
        T? last = null;
        foreach (var t in list)
        {
            last = t;
        }
        return last;
    }

    public static T? LastOrNull<T>(this IEnumerable<T> list, Func<T, bool> predicate) where T : struct
    {
        T? last = null;
        foreach (var t in list)
        {
            if (predicate(t)) last = t;
        }
        return last;
    }
}