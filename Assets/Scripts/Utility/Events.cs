﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using SerializableAttribute = System.SerializableAttribute;

[Serializable]
public class PieceEvent : UnityEvent<Piece> { };