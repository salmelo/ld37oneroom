﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct SceneField
{
#pragma warning disable 0649
    [SerializeField]
    private string path;
#pragma warning restore 0649

    public string Path
    {
        get
        {
            return path.Remove(path.Length - ".unity".Length).Substring("Assets/".Length);
        }
    }

    public string FullPath
    {
        get { return path; }
    }

}
