﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Color ToColor(this SquareColor color)
    {
        switch (color)
        {
            case SquareColor.Red:
                return Color.red;
            case SquareColor.Yellow:
                return Color.yellow;
            case SquareColor.Green:
                return Color.green;
            case SquareColor.Cyan:
                return Color.cyan;
            case SquareColor.Blue:
                return Color.blue;
            case SquareColor.Magenta:
                return Color.magenta;
            default:
                throw new System.ArgumentException("Invalid SquareColor");
        }
    }

    public static SquareColor Complementary(this SquareColor color)
    {
        return (SquareColor)(((int)color + 3) % 6);
    }

    public static Coord Spin(this Coord coord, int rotation)
    {
        rotation = rotation % 4;
        switch (rotation)
        {
            case 0:
                return coord;
            case 1:
                return new Coord(coord.Y, -coord.X);
            case 2:
                return -coord;
            case 3:
                return new Coord(-coord.Y, coord.X);
            default:
                throw new System.InvalidOperationException("This shouldn't happen.");
        }
    }

    public static T SelectRandom<T>(this IList<IWeighted<T>> list)
    {
        if (list.Count <= 0) throw new System.ArgumentException("Cannot select from empty list.");

        float weightSum = 0;
        foreach (var t in list)
        {
            weightSum += t.Weight;
        }
        var select = Random.Range(0, weightSum);
        weightSum = 0;
        foreach (var t in list)
        {
            weightSum += t.Weight;
            if (select <= weightSum) return t.Item;
        }
        throw new System.InvalidOperationException("Random selection failed somehow?");
    }

    public static Bounds RectBounds(this RectTransform rect)
    {
        var corners = new Vector3[4];
        rect.GetWorldCorners(corners);
        var b = new Bounds(corners[0], Vector3.zero);
        foreach (var v in corners)
        {
            b.Encapsulate(v);
        }
        return b;
    }
}
