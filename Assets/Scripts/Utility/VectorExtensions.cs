﻿using UnityEngine;
using System.Collections;

public static class VectorExtensions
{

    public static Vector3 WithX(this Vector3 v, float x)
    {
        return new Vector3(x, v.y, v.z);
    }

    public static Vector3 WithY(this Vector3 v, float y)
    {
        return new Vector3(v.x, y, v.z);
    }

    public static Vector3 WithZ(this Vector3 v, float z)
    {
        return new Vector3(v.x, v.y, z);
    }

    public static Vector3 WithZ(this Vector2 v, float z)
    {
        return new Vector3(v.x, v.y, z);
    }

    public static Vector3 WithXY(this Vector3 v, float x, float y)
    {
        return new Vector3(x, y, v.z);
    }

    public static Vector3 WithXZ(this Vector3 v, float x, float z)
    {
        return new Vector3(x, v.y, z);
    }

    public static Vector3 WithYZ(this Vector3 v, float y, float z)
    {
        return new Vector3(v.x, y, z);
    }
}
